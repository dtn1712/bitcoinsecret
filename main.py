import time
import sys

from platforms import BTCEPlatform, CryptoTradePlatform, BterPlatform, CryptsyPlatform

from settings import LIST_PAIRS, START_FUND_BTC, GOAL_PROFIT_BTC
from settings import BTCE, CRYPTO_TRADE, CRYPTSY
from settings import TRANSACTION_TIME
from settings import BASE_CURRENCY
from settings import MAX_TRADE_AMOUNT

from computing import getArbitrageProfit, getWithdrawalFee

from utils import sendMail

btce = BTCEPlatform()
cryptoTrade = CryptoTradePlatform()
cryptsy = CryptsyPlatform()

listPlatforms = [ btce, cryptoTrade, cryptsy ]
platformsMap = { BTCE: btce, CRYPTO_TRADE: cryptoTrade, CRYPTSY: cryptsy }


# Find the move. 
# Input includes the current trading platform and current amount
# Output include the new trading platform, the currency to trade, and the arbitrage profit for that move
def findArbitrageMove(currentPlatform,currentAmount):		
	bestMove = { "platform": None, "currency": None, "arbitrage": 0 }
	print "Current Platform: " + currentPlatform.platformId + " --- Current Amount: " + str(currentAmount) + "\n"
	
	# Try all the currency pair and find the arbitrage of each of them
	for pair in LIST_PAIRS:
		maxProfit = 0
		bestPlatform = currentPlatform

		# Try all the platform for this pair
		for platform in listPlatforms:
			arbitrage = getArbitrageProfit(currentPlatform,platform,currentAmount,pair)
			#print "Platform: " + platform.platformId + "  --- Pair: " + pair + "  --- Arbitrage: " + str(arbitrage) 
			if arbitrage != None:
				if arbitrage > maxProfit:
					maxProfit = arbitrage
					bestPlatform = platform
		if bestMove['arbitrage'] < maxProfit:
			bestMove['platform'] = bestPlatform.platformId
			bestMove['currency'] = pair[0:pair.index("_")]
			bestMove['arbitrage'] = maxProfit
	print "Move: " + str(bestMove)
	return bestMove
		
# Estimate how long it take to achieve the goal amount
def estimateProfit(startPlatform):
	timeTake = 0
	goalAmount = GOAL_PROFIT_BTC
	currentAmount = START_FUND_BTC
	currentPlatform = startPlatform
	totalAmount = currentAmount
	while totalAmount < goalAmount:
		arbitrageMove = findArbitrageMove(currentPlatform,currentAmount)
		netGain = 0
		netLoss = 0

		# If the current platform have no arbitrage move, it have to switch to 
		# another platform to find the opportunity, it take withdrawal fee
		# as a net loss. Otherwise, it earn the netgain
		if arbitrageMove['platform'] == None:
			for platform in listPlatforms:
				if currentPlatform.platformId != platform.platformId:
					netLoss = getWithdrawalFee(currentPlatform,BASE_CURRENCY,currentAmount) * -1
					currentPlatform = platform
					break
		else:
			currentPlatform = platformsMap[arbitrageMove['platform']]
			netGain = currentAmount * (arbitrageMove['arbitrage'] / 100.0 ) 
		netChange = netGain + netLoss
		totalAmount += netChange
		currentAmount += netChange

		# Limit amount for transaction. If the amount is too large, it
		# is not easy to make transaction. Therefore, if it reach the
		# certain level, the current amount will be reset to the start amount
		if currentAmount > MAX_TRADE_AMOUNT[BASE_CURRENCY]:
			currentAmount = START_FUND_BTC
			totalAmount -= START_FUND_BTC
		timeTake += TRANSACTION_TIME

		# Print out the elapse time and total amount after that time duration
		print "Elapsed Time: " + str(timeTake) + " hours"
		print "Total Amount: " + str(totalAmount)
		print "\n------------------\n"
	print "Goal: " + str(goalAmount) + " -- Time take (hours): " + str(timeTake)


def main():
	startPlatform = btce
	try:
		choosePlatform = sys.argv[1]
		startPlatform = platformsMap[choosePlatform]
	except:
		pass
	estimateProfit(startPlatform)

if __name__ == "__main__":
	main()