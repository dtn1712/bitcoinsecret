import urllib2
import json
import smtplib

from settings import EMAIL_HOST, EMAIL_USERNAME, EMAIL_PASSWORD

def sendMail(from_email,to_email,msg):
 	server = smtplib.SMTP(EMAIL_HOST)
  	server.starttls()
   	server.login(EMAIL_USERNAME,EMAIL_PASSWORD)
  	server.sendmail(from_email,to_email,msg)
   	server.quit()

def getAPIdata(link):
	try:
		data = urllib2.urlopen(link)
		json_data = json.loads(data.read())
		return json_data
	except Exception as e:
		print e
	return {}