from selenium import webdriver
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import WebDriverWait 
from selenium.webdriver.support import expected_conditions as EC

from settings import GET_ELEMENT_TYPE
from settings import NAME, PARTIAL_LINK_TEXT, ID

class BaseWebAutomation(object):
	def __init__(self):
		self.driver = webdriver.Firefox()

	def openWebpage(self,url):
		self.driver.get(url)

	def getElement(self,getType,value):
		if getType == ID:
			return self.driver.find_element(by=GET_ELEMENT_TYPE[getType],value)
		else:
			return self.driver.find_element(GET_ELEMENT_TYPE[getType],value)

	def waitForElement(self,time,getType,value):
		return WebDriverWait(self.driver,time).until(EC.presence_of_element_located((GET_ELEMENT_TYPE[getType], value)))

	def enterInput(self,element,value):
		element.send_keys(value)

	def clickLink(self,element):
		element.click()

	def closeWebpage(self):
		self.driver.quit()


class BTCEWebAutomation(BaseWebAutomation):

	def __init__(self):
		super(BTCEWebAutomation, self).__init__()
		self.baseUrl = "https://btc-e.com/"
		self.currencyAddress = {}
		self.openWebpage(self.baseUrl)

	def login(self,email,password):
		emailElement = self.getElement(NAME,"email")
		emailElement.send_keys(email)
		passwordElement = self.getElement(NAME,"password")
		passwordElement.send_keys(password)
		loginLink = self.getElement(PARTIAL_LINK_TEXT,"Login")
		self.clickLink(loginLink)

	def logout(self):
		logoutLink = self.waitForElement(10,PARTIAL_LINK_TEXT,"Logout")
		self.clickLink(logoutLink)
		
	def deposit(self,currency,amount):
		print "deposit function"

	def withdraw(self,currency,amount):
		print "withdraw function"
				



