from settings import TRANSACTION_MEASUREMENT_UNCERTAINTY

def getDifferencePercentage(base,target):
	return ( ( (float(target) - float(base)) / float(base) ) * 100 )


def getWithdrawalFee(platform,currency,withdrawAmount):
	percentFee = platform.withdrawalFeePercent[currency]
	amountFee = platform.withdrawalFeeAmount[currency]
	if amountFee != None and percentFee != None:
		return ( (withdrawAmount * percentFee) / 100.0 ) + amountFee		
	return None

def getBuyingAmount(platform,fundAmount,pair):
	pairPrice = platform.getPairPrice(pair)
	buyingFee = platform.transactionFee['buy']
	#print "BUYING - Price: " + str(pairPrice) + " -- Fee: " + str(buyingFee) + " -- Fund Amount: " + str(fundAmount)
	amountCurrency = float(fundAmount) / float(pairPrice) 
	amountAfterFee = amountCurrency - ( ( amountCurrency * buyingFee ) / 100 )
	return amountAfterFee - ( ( amountCurrency * TRANSACTION_MEASUREMENT_UNCERTAINTY ) / 100 )


def getSellingAmount(platform,fundAmount,pair):
	pairPrice = platform.getPairPrice(pair)
	sellingFee = platform.transactionFee['sell']
	#print "SELLING - Price: " + str(pairPrice) + " -- Fee: " + str(sellingFee) + " -- Fund Amount: " + str(fundAmount)
	amountCurrency = float(fundAmount) * float(pairPrice) 
	amountAfterFee = amountCurrency - ( ( amountCurrency * sellingFee ) / 100 )
	return amountAfterFee - ( ( amountCurrency * TRANSACTION_MEASUREMENT_UNCERTAINTY ) / 100 )


def getArbitrageProfit(currentPlatform,targetPlatform,baseAmount,pair):
	if currentPlatform.platformId == targetPlatform.platformId:
		return 0
	else:
		try:
			buyingAmount = getBuyingAmount(currentPlatform,baseAmount,pair)
			#print "Buying Amount: " + str(buyingAmount)
			withdrawalFee = getWithdrawalFee(currentPlatform,pair[0:pair.index("_")],buyingAmount)
			#print "Widthdrawal Fee: " + str(withdrawalFee)
			if withdrawalFee != None:
				sellingAmount = getSellingAmount(targetPlatform,buyingAmount - withdrawalFee,pair)
				#print "Selling Amount: " + str(sellingAmount)
				return getDifferencePercentage(baseAmount,sellingAmount)
			else:
				print "This pair contain currency which cannot be withdraw with this current platform"
				return None
		except Exception as e:
			return None
