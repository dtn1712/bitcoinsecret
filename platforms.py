import btceapi

from utils import getAPIdata

from settings import BTC, LTC, XPM, FTC, TRC, NMC, PPC
from settings import LIST_PAIRS
from settings import CRYPTSY_TICKER_LINK, CRYPTSY_MARKET_ID
from settings import CRYPTO_TRADE_TICKER_LINK, BTER_TICKER_LINK
from settings import BTCE, CRYPTO_TRADE, CRYPTSY, BTER


# Global variable
btcEConnection = btceapi.BTCEConnection()

# Abstract Class
class BaseTradingPlatformAPI(object):

	def __init__(self):
		self.listPair = LIST_PAIRS
		self.pairPrice = {}
		for pair in self.listPair:
			self.pairPrice[pair] = None


# BTCE API extend class
class BTCEPlatform(BaseTradingPlatformAPI):	

	def __init__(self):
		super(BTCEPlatform, self).__init__()
		self.platformId = BTCE 
		self.transactionFee = { 'buy': 0.2, 'sell': 0.2 } 
		self.withdrawalFeePercent = { BTC: 0, LTC: 0, XPM: 0, FTC: 0, TRC: 0, NMC: 0, PPC: 0 }
		self.withdrawalFeeAmount = { BTC: 0.001, LTC: 0.1, XPM: 0.1, FTC: 0.1, TRC: 0.1, NMC: 0.1, PPC: 0.1 }

	def getAllPairPrice(self):
		for pair in self.listPair:
			try:
				ticker = btceapi.getTicker(pair,btcEConnection)
				self.pairPrice[pair] = getattr(ticker,'last')
			except Exception as e:
				print e
		return self.pairPrice		

	def getPairPrice(self, pair):
		try:
			ticker = btceapi.getTicker(pair,btcEConnection)
			self.pairPrice[pair] = getattr(ticker,'last')
			return self.pairPrice[pair]
		except Exception as e:
			print e
		return None
		
	
			
# Crypto Trade extend class
class CryptoTradePlatform(BaseTradingPlatformAPI):

	def __init__(self):
 		super(CryptoTradePlatform, self).__init__()
 		self.platformId = CRYPTO_TRADE
		self.transactionFee = { 'buy': 0.2, 'sell': 0.2 } 
		self.withdrawalFeePercent = { BTC: 0, LTC: 0, XPM: 0, FTC: 0, TRC: 0, NMC: 0, PPC: 0 }
		self.withdrawalFeeAmount = { BTC: 0.001, LTC: 0.05, XPM: 0.01, FTC: 0.01, TRC: 0.01, NMC: 0.01, PPC: 0.01 }

	def getAllPairPrice(self):
		for pair in self.listPair:
			try:
				link = CRYPTO_TRADE_TICKER_LINK + pair
				json_data = getAPIdata(link)
				self.pairPrice[pair] = json_data['data']['last']
			except Exception as e:
				print e
		return self.pairPrice


	def getPairPrice(self,pair):
		try:
			link = CRYPTO_TRADE_TICKER_LINK + pair
			json_data = getAPIdata(link)
			self.pairPrice[pair] = json_data['data']['last']
			return self.pairPrice[pair]
		except Exception as e:
			print e
		return None


class CryptsyPlatform(BaseTradingPlatformAPI):

	def __init__(self):
		super(CryptsyPlatform, self).__init__()
		self.platformId = CRYPTSY
		self.transactionFee = { 'buy': 0.2, 'sell': 0.3 } 
		self.withdrawalFeePercent = { BTC: 0, LTC: 0, XPM: 0, FTC: 0, TRC: 0, NMC: 0, PPC: 0 }
		self.withdrawalFeeAmount = { BTC: 0.0002, LTC: 0.03, XPM: 0.01, FTC: 0.01, TRC: 0.015, NMC: 0.015, PPC: 0.02 }	

	def getAllPairPrice(self):
		for pair in self.listPair:
			try:
				link = CRYPTSY_TICKER_LINK + CRYPTSY_MARKET_ID[pair]
				json_data = getAPIdata(link)
				self.pairPrice[pair] = json_data['return']['markets'].values()[0]['lasttradeprice']
			except Exception as e:
				print e
		return self.pairPrice

	def getPairPrice(self,pair):
		try:
			link = CRYPTSY_TICKER_LINK + CRYPTSY_MARKET_ID[pair]
			json_data = getAPIdata(link)
			self.pairPrice[pair] = json_data['return']['markets'].values()[0]['lasttradeprice']
			return self.pairPrice[pair]
		except Exception as e:
			print e
		return None
	

class BterPlatform(BaseTradingPlatformAPI):

	def __init__(self):
		super(BterPlatform, self).__init__()
		self.platformId = BTER
		self.transactionFee = { 'buy': 0.2, 'sell': 0.2 } 	
		self.withdrawalFeePercent = { BTC: 0, LTC: 0, XPM: 0, FTC: 0.2, TRC: None, NMC: 0, PPC: 0 }
		self.withdrawalFeeAmount = { BTC: 0.0005, LTC: 0.02, XPM: 0.1, FTC: 0.1, TRC: None, NMC: 0.001, PPC: 0.001 }

	def getAllPairPrice(self):
		for pair in self.listPair:
			try:
				link = BTER_TICKER_LINK + pair
				json_data = getAPIdata(link)
				self.pairPrice[pair] = json_data['last']
			except Exception as e:
				print e
		return self.pairPrice


	def getPairPrice(self,pair):
		try:
			link = BTER_TICKER_LINK + pair
			json_data = getAPIdata(link)
			self.pairPrice[pair] = json_data['last']
			return self.pairPrice[pair]
		except Exception as e:
			print e
		return None

