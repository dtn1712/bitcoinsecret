from selenium.webdriver.common.by import By

# Pair
LTC_BTC = "ltc_btc"
PPC_BTC = "ppc_btc"
XPM_BTC = "xpm_btc"
FTC_BTC = "ftc_btc"
NMC_BTC = "nmc_btc"
TRC_BTC = "trc_btc"

# Coin 
BTC = "btc"
LTC = "ltc"
XPM = "xpm"
FTC = "ftc"
NMC = "nmc"
TRC = "trc"
PPC = "ppc"

# Platforms
BTCE = "btce"
CRYPTO_TRADE = "crypto"
CRYPTSY = "cryptsy"
BTER = "bter"

ID = "id"
TAG = "tag"
NAME = "name"
CLASS = "class"
CSS = "css"
LINK_TEXT = "link_text"
PARTIAL_LINK_TEXT = "partial_link_text"
XPATH = "xpath"


LIST_PAIRS = [ LTC_BTC, PPC_BTC, XPM_BTC, FTC_BTC, NMC_BTC, TRC_BTC ]

CRYPTO_TRADE_TICKER_LINK = "https://crypto-trade.com/api/1/ticker/"
BTER_TICKER_LINK = "https://bter.com/api/1/ticker/"

CRYPTSY_TICKER_LINK = "http://pubapi.cryptsy.com/api.php?method=singlemarketdata&marketid="
CRYPTSY_MARKET_ID = { 
						LTC_BTC: '3', 
						PPC_BTC: '28', 
						XPM_BTC: '63', 
						FTC_BTC: '5', 
						NMC_BTC: '29', 
						TRC_BTC: '27' 
					}

EMAIL_USERNAME = "dang.nguyen.test"
EMAIL_PASSWORD = "HelloWorld123"

EMAIL_HOST = "smtp.gmail.com:587"

START_FUND_BTC = 1
GOAL_PROFIT_BTC = 100

TRANSACTION_TIME = 1 	# in hours

BASE_CURRENCY = BTC

MAX_TRADE_AMOUNT = 	{ 
						BTC: 10, 
						LTC: 200, 
						NMC: 500, 
						XPM: 1000, 
						FTC: 1000, 
						TRC: 1000, 
						PPC: 1000 
					}

TRANSACTION_MEASUREMENT_UNCERTAINTY = 0.1	# in percentage 

GET_ELEMENT_TYPE = 	{
						ID: By.ID,
						TAG: By.TAG_NAME,
						NAME: By.NAME,
						CLASS: By.CLASS_NAME,
						CSS: By.CSS_SELECTOR,
						LINK_TEXT: By.LINK_TEXT,
						PARTIAL_LINK_TEXT: By.PARTIAL_LINK_TEXT,
						XPATH: By.XPATH
					}